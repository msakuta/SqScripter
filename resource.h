#ifndef IDC_STATIC
#define IDC_STATIC (-1)
#endif

#define IDC_CLEARCONSOLE                        1001
#define IDC_SCRIPTEDIT                          1002
#define IDC_COMMAND                             1003
#define IDC_CONSOLE                             1013
#define IDM_SCRIPT_OPEN                         40000
#define IDM_WHITESPACES                         40001
#define IDM_LINENUMBERS                         40002
